package com.hsbc.da1.model;

public class FoodItems implements Comparable<FoodItems> {
	private long itemId;
	private String itemName;
	private double UnitPrice;
	private String DateOfManufacture;
	private String DateOfExpiry;
	private static int counter=1000;
	private int quantity;
	private String veg;
	public FoodItems(String itemName,double UnitPrice,String DateOfManufacture,String DateOfExpiry,String veg,int quantity) {
		
		this.itemName=itemName;
		this.UnitPrice=UnitPrice;
		this.DateOfManufacture=DateOfManufacture;
		this.DateOfExpiry=DateOfExpiry;
		this.itemId=++counter;
		this.veg=veg;
		this.quantity=quantity;
		
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getUnitPrice() {
		return UnitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		UnitPrice = unitPrice;
	}

	public String getDateOfManufacture() {
		return DateOfManufacture;
	}

	public void setDateOfManufacture(String dateOfManufacture) {
		DateOfManufacture = dateOfManufacture;
	}

	public String getDateOfExpiry() {
		return DateOfExpiry;
	}

	public void setDateOfExpiry(String dateOfExpiry) {
		DateOfExpiry = dateOfExpiry;
	}

	public long getItemId() {
		return itemId;
	}
	

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (itemId ^ (itemId >>> 32));
		result = prime * result + ((itemName == null) ? 0 : itemName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FoodItems other = (FoodItems) obj;
		if (itemId != other.itemId)
			return false;
		if (itemName == null) {
			if (other.itemName != null)
				return false;
		} else if (!itemName.equals(other.itemName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FoodItems [itemId=" + itemId + ", itemName=" + itemName + ", UnitPrice=" + UnitPrice
				+ ", DateOfManufacture=" + DateOfManufacture + ", DateOfExpiry=" + DateOfExpiry + ", quantity="
				+ quantity + ", veg=" + veg + "]";
	}

	@Override
	public int compareTo(FoodItems obj) {
		// TODO Auto-generated method stub
		return obj.getQuantity()-this.getQuantity();
	}

	
	
	

}
