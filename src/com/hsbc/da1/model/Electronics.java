package com.hsbc.da1.model;

public class Electronics implements Comparable<Electronics> {
	
	private long itemId;
	private String itemName;
	private double UnitPrice;
	private int quantity;
	private int warranty;
	private int counter=2000;

	public Electronics(String itemName,double Unitprice,int quantity,int warranty) {
		
		this.itemId=++counter;
		this.itemName=itemName;
		this.UnitPrice=Unitprice;
		this.quantity=quantity;
		this.warranty=warranty;
		
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getUnitPrice() {
		return UnitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		UnitPrice = unitPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getWarranty() {
		return warranty;
	}

	public void setWarranty(int warranty) {
		this.warranty = warranty;
	}

	public long getItemId() {
		return itemId;
	}

	@Override
	public String toString() {
		return "Electronics [itemId=" + itemId + ", itemName=" + itemName + ", UnitPrice=" + UnitPrice + ", quantity="
				+ quantity + ", warranty=" + warranty + ", counter=" + counter + "]";
	}

	@Override
	public int compareTo(Electronics obj) {
		// TODO Auto-generated method stub
		return obj.getQuantity()-this.getQuantity();
	}
	
}
