package com.hsbc.da1.model;

public class Apparel implements Comparable<Apparel> {
	
	private long itemId;
	private String itemName;
	private double UnitPrice;
	private String size;
	private String material;
	private int quantity;
	private int counter=100;
	
	public Apparel(String itemName,double UnitPrice,String size,String material,int quantity) {
		
		this.itemName=itemName;
		this.itemId=++counter;
		this.UnitPrice=UnitPrice;
		this.size=size;
		this.material=material;
		this.quantity=quantity;
		
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getUnitPrice() {
		return UnitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		UnitPrice = unitPrice;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public long getItemId() {
		return itemId;
	}

	@Override
	public String toString() {
		return "Apparel [itemId=" + itemId + ", itemName=" + itemName + ", UnitPrice=" + UnitPrice + ", size=" + size
				+ ", material=" + material + ", quantity=" + quantity + ", counter=" + counter + "]";
	}

	@Override
	public int compareTo(Apparel obj) {
		// TODO Auto-generated method stub
		return  obj.getQuantity()-this.getQuantity();
	}
	

}
