package com.hsbc.da1.controller;

import java.util.Set;

import com.hsbc.da1.exception.ItemListempty;
import com.hsbc.da1.model.Apparel;
import com.hsbc.da1.model.Electronics;
import com.hsbc.da1.model.FoodItems;
import com.hsbc.da1.service.ItemService;
import com.hsbc.da1.service.ItemServiceImpl;

public class ItemController {
	
	private ItemService service=new ItemServiceImpl();
	public FoodItems saveFoodItem(String itemName,double UnitPrice,String DateOfManufacture,String DateOfExpiry,String veg,int quantity) {
		return this.service.saveFoodItem(itemName, UnitPrice, DateOfManufacture, DateOfExpiry, veg, quantity);
	}
	public Electronics saveElectronicsItem(String itemName,double Unitprice,int quantity,int warranty) {
		return this.service.saveElectronicsItem(itemName, Unitprice, quantity, warranty);
	}
	public Apparel saveApparelItem(String itemName,double UnitPrice,String size,String material,int quantity) {
		return this.service.saveApparelItem(itemName, UnitPrice, size, material, quantity);
	}
	public Set<FoodItems> fetchFoodItems() throws ItemListempty{
		return this.service.fetchFoodItems();
	}
	public Set<Apparel>   fetchApparel() throws ItemListempty{
		return this.service.fetchApparel();
	}
	public Set<Electronics>   fetchElectronics()throws  ItemListempty{
		return this.service.fetchElectronics();
	}

}
