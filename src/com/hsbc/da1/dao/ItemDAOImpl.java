package com.hsbc.da1.dao;

import java.util.Set;
import java.util.TreeSet;

import com.hsbc.da1.exception.ItemListempty;
import com.hsbc.da1.model.Apparel;
import com.hsbc.da1.model.Electronics;
import com.hsbc.da1.model.FoodItems;

public class ItemDAOImpl implements  ItemsDAO {
    Set<FoodItems> foodset=new TreeSet<>();
	Set<Electronics> electronicsSet=new TreeSet<>();
	Set<Apparel> apparelSet=new TreeSet<>();
	
	@Override
	public FoodItems saveFoodItem(FoodItems item) {
		// TODO Auto-generated method stub
		foodset.add(item);
		
		return item;
	}

	@Override
	public Electronics saveElectronicsItem(Electronics item) {
		// TODO Auto-generated method stub
		electronicsSet.add(item);
		return item;
	}

	@Override
	public Apparel saveApparelItem(Apparel item) {
		// TODO Auto-generated method stub
		apparelSet.add(item);
		return item;
	}

	@Override
	public Set<FoodItems> fetchFoodItems() throws ItemListempty {
		// TODO Auto-generated method stub
		if(foodset.size()>0) {
		return foodset;
		}
		throw new ItemListempty("no item found");
	}

	@Override
	public Set<Apparel> fetchApparel() throws ItemListempty {
		// TODO Auto-generated method stub
		if(apparelSet.size()>0) {
			return apparelSet;
		}
		throw new ItemListempty("no item found");
	}

	@Override
	public Set<Electronics> fetchElectronics() throws ItemListempty{
		// TODO Auto-generated method stub
		if(electronicsSet.size()>0) {
			return electronicsSet;
		}
		throw new ItemListempty("no item found");
	}

}
