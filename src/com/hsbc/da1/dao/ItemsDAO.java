package com.hsbc.da1.dao;

import java.util.Set;

import com.hsbc.da1.exception.ItemListempty;
import com.hsbc.da1.model.Apparel;
import com.hsbc.da1.model.Electronics;
import com.hsbc.da1.model.FoodItems;

public interface ItemsDAO {
	
	public FoodItems saveFoodItem(FoodItems item);
	public Electronics saveElectronicsItem(Electronics item);
	public Apparel saveApparelItem(Apparel item);
	public Set<FoodItems> fetchFoodItems() throws ItemListempty;
	public Set<Apparel>   fetchApparel() throws ItemListempty;
	public Set<Electronics>   fetchElectronics() throws ItemListempty;
	

}
