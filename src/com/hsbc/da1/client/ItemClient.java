package com.hsbc.da1.client;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import com.hsbc.da1.controller.ItemController;
import com.hsbc.da1.exception.ItemListempty;
import com.hsbc.da1.model.Apparel;
import com.hsbc.da1.model.Electronics;
import com.hsbc.da1.model.FoodItems;



public class ItemClient {
	public static void main(String[] args) {
		
		System.out.println(" **********enter the Item type you want to choose*******");
		System.out.println(" ******* 1-->FOOD ITEMS");
		System.out.println(" *******2---> APPAREL ITEMS");
		System.out.println(" ******3------>ELECTRONICS");
		
		Scanner sn=new Scanner(System.in);
		int option;
		option=sn.nextInt();
		
		Set<FoodItems> foodItemsSet=new TreeSet<>();
		Set<Apparel> apparelItemsSet=new TreeSet<>();
		Set<Electronics> electronicsItemsSet=new TreeSet<>();
		
		ItemController controller=new ItemController();
		FoodItems milk=controller.saveFoodItem("MILK", 20, "20/08/97","22/08/97" , "yes", 50);
		FoodItems bread=controller.saveFoodItem("bread", 26, "20/09/97","02/10/97" , "yes", 70);
		FoodItems chicken=controller.saveFoodItem("chicken", 200, "20/09/97","14/10/97" , "no", 100);
		Apparel shirt=controller.saveApparelItem("shirt", 200, "m", "silk", 30);
		Apparel pant=controller.saveApparelItem("pant", 1000, "l", "jeans", 100);
		Apparel jacket=controller.saveApparelItem("jacket", 4000, "l", "wool", 20);
		Electronics mouse=controller.saveElectronicsItem("mouse", 400, 50, 12);
		Electronics tv=controller.saveElectronicsItem("tv", 50_000, 70, 24);
		Electronics laptop=controller.saveElectronicsItem("laptop", 1_00_000, 20, 24);
		try {
		 foodItemsSet=controller.fetchFoodItems();
		 apparelItemsSet=controller.fetchApparel();
		 electronicsItemsSet=controller.fetchElectronics();
		
		}catch(ItemListempty exception) {
			System.out.println(exception.getMessage());
		}
		switch(option) {
		
		case 1:
			Iterator<FoodItems> it=foodItemsSet.iterator();
			for(int i=0;i<3;i++) {
				System.out.println(it.next().toString());
			}
			break;
		
		case 2:
			Iterator<Apparel> it1=apparelItemsSet.iterator();
			for(int i=0;i<3;i++) {
				System.out.println(it1.next().toString());
			}
			break;
		case 3:
			Iterator<Electronics> it2=electronicsItemsSet.iterator();
			for(int i=0;i<3;i++) {
				System.out.println(it2.next().toString());
			}
		break;
		
		}
		
		
		
		
		
	}
	

}
