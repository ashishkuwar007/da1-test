package com.hsbc.da1.service;

import java.util.Set;

import com.hsbc.da1.dao.ItemDAOImpl;
import com.hsbc.da1.dao.ItemsDAO;
import com.hsbc.da1.exception.ItemListempty;
import com.hsbc.da1.model.Apparel;
import com.hsbc.da1.model.Electronics;
import com.hsbc.da1.model.FoodItems;

public class ItemServiceImpl implements ItemService{
	
	private ItemsDAO dao=new ItemDAOImpl();

	@Override
	public FoodItems saveFoodItem(String itemName,double UnitPrice,String DateOfManufacture,String DateOfExpiry,String veg,int quantity) {
		// TODO Auto-generated method stub
		FoodItems item=new FoodItems(itemName, UnitPrice, DateOfManufacture, DateOfExpiry, veg, quantity);
		return this.dao.saveFoodItem(item);
	}

	@Override
	public Electronics saveElectronicsItem(String itemName,double Unitprice,int quantity,int warranty) {
		// TODO Auto-generated method stub
		Electronics item=new Electronics(itemName, Unitprice, quantity, warranty);
		return this.dao.saveElectronicsItem(item);
	}

	@Override
	public Apparel saveApparelItem(String itemName,double UnitPrice,String size,String material,int quantity) {
		// TODO Auto-generated method stub
		Apparel item=new Apparel(itemName, UnitPrice, size, material, quantity);
		return this.dao.saveApparelItem(item);
	}

	@Override
	public Set<FoodItems> fetchFoodItems() throws ItemListempty {
		// TODO Auto-generated method stub
		return this.dao.fetchFoodItems();
	}

	@Override
	public Set<Apparel> fetchApparel() throws ItemListempty {
		// TODO Auto-generated method stub
		return this.dao.fetchApparel();
	}

	@Override
	public Set<Electronics> fetchElectronics() throws ItemListempty {
		// TODO Auto-generated method stub
		return this.dao.fetchElectronics();
	}

}
