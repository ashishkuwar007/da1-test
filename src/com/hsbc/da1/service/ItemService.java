package com.hsbc.da1.service;

import java.util.Set;

import com.hsbc.da1.exception.ItemListempty;
import com.hsbc.da1.model.Apparel;
import com.hsbc.da1.model.Electronics;
import com.hsbc.da1.model.FoodItems;

public interface ItemService {

	public FoodItems saveFoodItem(String itemName,double UnitPrice,String DateOfManufacture,String DateOfExpiry,String veg,int quantity);
	public Electronics saveElectronicsItem(String itemName,double Unitprice,int quantity,int warranty);
	public Apparel saveApparelItem(String itemName,double UnitPrice,String size,String material,int quantity);
	public Set<FoodItems> fetchFoodItems() throws ItemListempty;
	public Set<Apparel>   fetchApparel() throws ItemListempty;
	public Set<Electronics>   fetchElectronics() throws ItemListempty;
	
}
